#include<stdio.h>
#include<stdlib.h>
int tablero[15][15];
int fin;

void imprimir_tablero (){
	int i,j;
	for(i=0,j=0;i<14;i++){
	for(j=0;j<14;j++){
		if(tablero[i][j]==1){
		printf("%c%c",'*',32);	
		}else if(tablero[i][j]==2){
		printf("%c%c",'#',32);	
		} else{
		printf("%d%c",tablero[i][j],32);	
		}
		}
		printf("\n");
	}
}

    bool contar(int r, int dr, int c, int dc) {
        int jugador = tablero[r][c];  
        for (int i = 1; i < 5; i++) {
            if (tablero[r+dr*i][c+dc*i] != jugador) return false;
        }
        return true;  
    }
    
int comprobar_ganador(){
	int i,j;
	   for (i = 0; i < 14; i++) {
            for (j = 0; j < 14; j++) {
                int p = tablero[i][j];    
                    if (i < 10) 
                        if (contar(i, 1, j, 0)) return p;
                    if (j < 10) { 
                        if (contar(i, 0, j, 1))  return p;
                        if (i < 10) { 
                            if (contar(i, 1, j, 1)) return p;
                        }
                    }
                    if (j > 3 && i < 10) { 
                        if (contar(i, 1, j, -1)) return p;
                    }
                  return 0;  
            }
        }
}

void juego(int x, int y, int jugador){
	if(comprobar_ganador() == 0){
		tablero[y][x]=jugador;
	} 
	else{
		printf("El ganador es el jugador %d",comprobar_ganador());
		fin = 1;
	}
}

main (){
	int	x,y; 
	int jugador=rand()%2;
	while(fin == 0){
	if(jugador == 0){
		x=0,y=0;
		printf("Jugador 1\n");
		printf("Introduce X:");
		scanf("%d",&x);
		printf("Introduce Y:");
		scanf("%d",&y);
		if(tablero[y][x] == 0){
		juego(x,y,1);
		jugador=1;
	}
	}
	else if(jugador == 1){
		x=0,y=0;
		printf("Jugador 2\n");
		printf("Introduce X:");
		scanf("%d",&x);
		printf("Introduce Y:");
		scanf("%d",&y);
		if(tablero[y][x] == 0){
		juego(x,y,2);
		jugador=0;
	}
	}
	imprimir_tablero();
	}
}

