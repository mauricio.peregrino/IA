/* Practica 1 IA (9-11) Mauricio Peregrino S�nchez*/
#include <stdio.h>
	void menu(int * circulo, int * direccion);
	void imprimirTablero();
	void movimiento(int circulo, int direccion);
						//***** 'o'= canica verde, '*' = canica roja, '-'= nada
		char tablero[4][7]={{'-','*','-','o','-','o','-'},
							{'*','-','o','-','*','-','o'},	
							{'-','*','-','o','-','*','-'},
							{'-','-','o','-','*','-','-'}	
							};
		char tableroCopia[4][7];
		main(){
			imprimirTablero();
	while(1){
		int circulo,direccion;
		menu(&circulo,&direccion);
		movimiento(circulo,direccion);
		imprimirTablero(tablero);
	}
}
void menu(int * circulo, int * direccion){
	printf("Indica el c%crculo que deseas girar\n",161);
	printf("Izquierdo (1) \n");
	printf("Derecho (2) \n");
	printf("Abajo (3) \n");
	scanf("%d",circulo);
	printf("Indica la direcci%cn\n",162);
	printf("Izquierda (1) \n");
	printf("Derecha (2) \n");
	scanf("%d",direccion);
}
void imprimirTablero(){
	int x=0,y=0;
	for(x=0,y=0;y<4;y++){
		for(x=0;x<7;x++){
			if(tablero[y][x] == '-'){
			      printf("%c%c",32,32);
			}
			else{
			printf(" %c",tablero[y][x]);	
			}
		}
		printf("\n");
	}
}
void igualar(){
	int x=0,y=0;
	for(x=0,y=0;y<4;y++){
		for(x=0;x<7;x++){
		tableroCopia[y][x]=tablero[y][x];
		}
	}
}
void movimiento(int circulo, int direccion){
	igualar();
	if(circulo == 1){
		if(direccion == 1){
			tablero[0][1]=tableroCopia[0][3];
			tablero[0][3]=tableroCopia[1][4];
			tablero[1][4]=tableroCopia[2][3];
			tablero[2][3]=tableroCopia[2][1];
			tablero[2][1]=tableroCopia[1][0];
			tablero[1][0]=tableroCopia[0][1];
		}else{
			tablero[0][1]=tableroCopia[1][0];
			tablero[0][3]=tableroCopia[0][1];
			tablero[1][4]=tableroCopia[0][3];
			tablero[2][3]=tableroCopia[1][4];
			tablero[2][1]=tableroCopia[2][3];
			tablero[1][0]=tableroCopia[2][1];
		}
	}
	if(circulo == 2){
		if(direccion == 1){
			tablero[0][3]=tableroCopia[0][5];
			tablero[0][5]=tableroCopia[1][6];
			tablero[1][6]=tableroCopia[2][5];
			tablero[2][5]=tableroCopia[2][3];
			tablero[2][3]=tableroCopia[1][2];
			tablero[1][2]=tableroCopia[0][3];
		}else{
			tablero[0][3]=tableroCopia[1][2];
			tablero[0][5]=tableroCopia[0][3];
			tablero[1][6]=tableroCopia[0][5];
			tablero[2][5]=tableroCopia[1][6];
			tablero[2][3]=tableroCopia[2][5];
			tablero[1][2]=tableroCopia[2][3];
		}
	}
	if(circulo == 3){
		if(direccion == 1){
			tablero[1][2]=tableroCopia[1][4];
			tablero[1][4]=tableroCopia[2][5];
			tablero[2][5]=tableroCopia[3][4];
			tablero[3][4]=tableroCopia[3][2];
			tablero[3][2]=tableroCopia[2][1];
			tablero[2][1]=tableroCopia[1][2];
		}else{
			tablero[1][2]=tableroCopia[2][1];
			tablero[1][4]=tableroCopia[1][2];
			tablero[2][5]=tableroCopia[1][4];
			tablero[3][4]=tableroCopia[2][5];
			tablero[3][2]=tableroCopia[3][4];
			tablero[2][1]=tableroCopia[3][2];
		}
	}
}
