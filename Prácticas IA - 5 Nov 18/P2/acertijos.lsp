(defvar frase '())
(defvar resultado '())
(defvar bandera 0)
;Contextos:
(defparameter *familiar* (list 'madre 'hijos 'hermanos))
(defparameter *dias* (list 'hoy 'maniana 'ayer 'dia 'lunes 'martes 'miercoles 'jueves 'viernes))
(defparameter *secuencias* (list 'primero 'segundo 'tercero 'cuarto))
(defparameter *vaqueros* (list 'vaquero))
(defparameter *locaciones* (list 'pueblo))
(defparameter *acciones* (list 'tiene 'llama 'dijo 'llega 'queda 'viven 'son 'parecen 'venir 'fue 'existe 'llaman))


(defun main()

(princ "Ingresa la frase para averiguar su contexto")
(setq frase (read-line))
(setq lista (convertir frase))

(recorrer lista)
(princ resultado)
)

(defun convertir (s)
  (let ((L (read-from-string 
           (concatenate 'string "(" s ")"))))
    L))
	
(defun contar (a L)
  (cond
   ((null L) 0)
   ((equal a (car L)) (+ 1 (contar a (cdr L))))
  (t (contar a (cdr L)))))
  
(defun recorrer(lista)
(if (cdr lista)
(progn 
(if (> (contar (car lista) *familiar*) 0)
(setq resultado (cons 'familiar- resultado))
)

(if (> (contar (car lista) *dias*) 0)
(setq resultado (cons 'dias- resultado))
)

(if (> (contar (car lista) *secuencias*) 0)
(setq resultado (cons 'secuencias- resultado))
)

(if (> (contar (car lista) *vaqueros*) 0)
(setq resultado (cons 'vaqueros- resultado))
)

(if (> (contar (car lista) *locaciones*) 0)
(setq resultado (cons 'locaciones- resultado))
)
(if (> (contar (car lista) *acciones*) 0)
(setq resultado (cons 'acciones- resultado))
)
(recorrer (cdr lista))
)
(progn
(if (> (contar (car lista) *familiar*) 0)
(setq resultado (cons 'familiar resultado))
)

(if (> (contar (car lista) *dias*) 0)
(setq resultado (cons 'dias resultado))
)

(if (> (contar (car lista) *secuencias*) 0)
(setq resultado (cons 'secuencias resultado))
)

(if (> (contar (car lista) *vaqueros*) 0)
(setq resultado (cons 'vaqueros resultado))
)

(if (> (contar (car lista) *locaciones*) 0)
(setq resultado (cons 'locaciones resultado))
)
(if (> (contar (car lista) *acciones*) 0)
(setq resultado (cons 'acciones resultado))
)
)
)


)