/* Programa que implementa el Algoritmo A asterisco*/
/* Alumno: Mauricio Peregrino */
/* IA (11-12)*/ 
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#define G 10
	char * listaAbierta[100];
	char * listaCerrada[100];
	char * tablero [9][10] ={
							{" "," "," "," "," "," "," "," "," "," "},
							{" ","A"," ","J","O","Q","W","Y","CC"," "},
							{" ","B"," ","K"," "," "," ","Z","DD"," "},
							{" ","C"," ","L"," ","R"," "," ","EE"," "},
							{" ","D","H","M","P","S"," "," ","FF"," "},
							{" ","E"," ","N"," ","T"," ","AA","GG"," "},
							{" ","F"," "," "," ","U"," "," ","HH"," "},
							{" ","G","I","�"," ","V","X","BB","*x*"," "},
							{" "," "," "," "," "," "," "," "," "," "}};
	int x=1,y=1;	//posicion actual (0,0), pero por los muros de contorno hay que ponerle (1,1)
	int cont,cont2;
struct posicion {
	int g;
	int h;
	int f;
	char * letra;		//hacia donde apunta (nodo padre)?
};
struct posicion posiciones[9][10];
void imprimirTablero();  //tablero con las letras
void imprimirTablero2(); // tablero donde se vera la solucion del problema
bool izquierda();
bool derecha();
bool arriba();
bool abajo();
bool noreste();
bool noroeste();
bool sureste();
bool suroeste();
void mover_izquierda();
void mover_derecha();
void mover_arriba();
void mover_abajo();
void mover_noreste();
void mover_noroeste();
void mover_sureste();
void mover_suroeste();
bool comparar_strings(int x, int y);
bool comparar_strings2(int x, int y);
int calcularH(int x1,int y1, int x2, int y2);
void Avanzar();
void Aasterisco();
main (){
	int pausa;
imprimirTablero();
Aasterisco();
scanf("%d", &pausa);
}


void imprimirTablero (){
	int i,j;
	for (i=0,j=0;i<9;i++){
		for(j=0;j<10;j++){
			if(tablero[i][j] == " "){
				printf("%c%c%c%c%c",178,32,32,32,32);
			} else{
				printf("%s%c%c%c%c",tablero[i][j],32,32,32,32);
			}
		}
		printf("\n\n");
	}
}
void imprimirTablero2 (){
	int i,j;
	for (i=0,j=0;i<9;i++){
		for(j=0;j<10;j++){
			if(tablero[i][j] == " "){
				printf("%c%c%c%c%c",178,32,32,32,32);
			} else{
				printf("%s%c%c%c%c",posiciones[i][j].letra,32,32,32,32);
			}
		}
		printf("\n\n");
	}
}


void Aasterisco (){
     cont = 0;							//contador para la lista Cerrada
     cont2 = 0;							//contador para la lista Abierta
	while(x != 8 && y != 7){ 			  //indicamos la coordenada hacia donde queremos llegar
		if(x == 1 && y == 1){ 			 // cuando es la primera iteracion
			*listaCerrada[cont] =  'a'; //tablero[y][x];
		  posiciones[y][x].g=0; posiciones[y][x].f=calcularH(x,y,8,7); posiciones[y][x].h=calcularH(x,y,8,7);  posiciones[y][x].letra=tablero[y][x]; // inicializando todo lo del nodo de inicio (A)
			cont++;
			if(izquierda() == true){
			listaAbierta[cont2]=tablero[y][x-1];
			cont2++;
			posiciones[y][x-1].g=posiciones[y][x].g + 10; posiciones[y][x-1].h=calcularH(x-1,y,8,7); 
			posiciones[y][x-1].f=posiciones[y][x-1].g + posiciones[y][x-1].h;   posiciones[y][x-1].letra= tablero[y][x];
			}
			if(derecha() == true){
			listaAbierta[cont2]=tablero[y][x+1];
			cont2++;
			posiciones[y][x+1].g=posiciones[y][x].g + 10; posiciones[y][x+1].h=calcularH(x+1,y,8,7); 
			posiciones[y][x+1].f=posiciones[y][x+1].g + posiciones[y][x+1].h;   posiciones[y][x+1].letra= tablero[y][x];				
			}
			if(arriba() == true){
			listaAbierta[cont2]=tablero[y+1][x];
			cont2++;
			posiciones[y+1][x].g=posiciones[y][x].g + 10; posiciones[y+1][x].h=calcularH(x,y+1,8,7); 
			posiciones[y+1][x].f=posiciones[y+1][x].g + posiciones[y+1][x].h;   posiciones[y+1][x].letra= tablero[y][x];				
			}
			if(abajo() == true){
			listaAbierta[cont2]=tablero[y-1][x];
			cont2++;
			posiciones[y-1][x].g=posiciones[y][x].g + 10; posiciones[y-1][x].h=calcularH(x,y-1,8,7); 
			posiciones[y-1][x].f=posiciones[y-1][x].g + posiciones[y-1][x].h;   posiciones[y-1][x].letra= tablero[y][x];				
			}
			if(noreste() == true){
			listaAbierta[cont2]=tablero[y+1][x+1];
			cont2++;
			posiciones[y+1][x+1].g=posiciones[y][x].g + 14; posiciones[y+1][x+1].h=calcularH(x+1,y+1,8,7); 
			posiciones[y+1][x+1].f=posiciones[y+1][x+1].g + posiciones[y+1][x+1].h;   posiciones[y+1][x+1].letra= tablero[y][x];				
			}
			if(noroeste() == true){
			listaAbierta[cont2]=tablero[y+1][x-1];
			cont2++;
			posiciones[y+1][x-1].g=posiciones[y][x].g + 14; posiciones[y+1][x-1].h=calcularH(x-1,y+1,8,7); 
			posiciones[y+1][x-1].f=posiciones[y+1][x-1].g + posiciones[y+1][x-1].h;   posiciones[y+1][x-1].letra= tablero[y][x];				
			}
			if(sureste() == true){
			listaAbierta[cont2]=tablero[y-1][x+1];
			cont2++;
			posiciones[y-1][x+1].g=posiciones[y][x].g + 14; posiciones[y-1][x+1].h=calcularH(x+1,y-1,8,7); 
			posiciones[y-1][x+1].f=posiciones[y-1][x+1].g + posiciones[y-1][x+1].h;   posiciones[y-1][x+1].letra= tablero[y][x];				
			}
			if(suroeste() == true){
				listaAbierta[cont2]=tablero[y-1][x-1];
			cont2++;
			posiciones[y-1][x-1].g=posiciones[y][x].g + 14; posiciones[y-1][x-1].h=calcularH(x-1,y-1,8,7); 
			posiciones[y-1][x-1].f=posiciones[y-1][x-1].g + posiciones[y-1][x-1].h;   posiciones[y-1][x-1].letra= tablero[y][x];			
			}
			void Avanzar();
		}
		else{
			if(izquierda() == true){
				if(comparar_strings2(x-1,y) == true){				//para saber si est� en la lista abierta
			listaAbierta[cont2]=tablero[y][x-1];
			cont2++;
			posiciones[y][x-1].g=posiciones[y][x].g + 10; posiciones[y][x-1].h=calcularH(x-1,y,8,7); 
			posiciones[y][x-1].f=posiciones[y][x-1].g + posiciones[y][x-1].h;   posiciones[y][x-1].letra= tablero[y][x];
			}
			else {				//si est� en la lista abierta entonces comparamos las f's y si es menor entonces recalculamos
				if(posiciones[y][x].g + 10 < posiciones[y][x-1].g){
			posiciones[y][x-1].g=posiciones[y][x].g + 10; posiciones[y][x-1].h=calcularH(x-1,y,8,7); 
			posiciones[y][x-1].f=posiciones[y][x-1].g + posiciones[y][x-1].h;   posiciones[y][x-1].letra= tablero[y][x];	
				}
			}
			}
			if(derecha() == true){
				if(comparar_strings2(x+1,y) == true){
			listaAbierta[cont2]=tablero[y][x+1];
			cont2++;
			posiciones[y][x+1].g=posiciones[y][x].g + 10; posiciones[y][x+1].h=calcularH(x+1,y,8,7); 
			posiciones[y][x+1].f=posiciones[y][x+1].g + posiciones[y][x+1].h;   posiciones[y][x+1].letra= tablero[y][x];
			}
				else {				//si est� en la lista abierta entonces comparamos las f's y si es menor entonces recalculamos
				if(posiciones[y][x].g + 10 < posiciones[y][x+1].g){
			posiciones[y][x+1].g=posiciones[y][x].g + 10; posiciones[y][x+1].h=calcularH(x+1,y,8,7); 
			posiciones[y][x+1].f=posiciones[y][x+1].g + posiciones[y][x+1].h;   posiciones[y][x+1].letra= tablero[y][x];
				}
			}
			}
			if(arriba() == true){
				if(comparar_strings2(x,y+1) == true){
			listaAbierta[cont2]=tablero[y+1][x];
			cont2++;
			posiciones[y+1][x].g=posiciones[y][x].g + 10; posiciones[y+1][x].h=calcularH(x,y+1,8,7); 
			posiciones[y+1][x].f=posiciones[y+1][x].g + posiciones[y+1][x].h;   posiciones[y+1][x].letra= tablero[y][x];	
			}
				else {				//si est� en la lista abierta entonces comparamos las f's y si es menor entonces recalculamos
				if(posiciones[y][x].g + 10 < posiciones[y+1][x].g){
			posiciones[y+1][x].g=posiciones[y][x].g + 10; posiciones[y+1][x].h=calcularH(x,y+1,8,7); 
			posiciones[y+1][x].f=posiciones[y+1][x].g + posiciones[y+1][x].h;   posiciones[y+1][x].letra= tablero[y][x];
				}
			}
			}
			if(abajo() == true){
				if(comparar_strings2(x,y-1) == true){
			listaAbierta[cont2]=tablero[y-1][x];
			cont2++;
			posiciones[y-1][x].g=posiciones[y][x].g + 10; posiciones[y-1][x].h=calcularH(x,y-1,8,7); 
			posiciones[y-1][x].f=posiciones[y-1][x].g + posiciones[y-1][x].h;   posiciones[y-1][x].letra= tablero[y][x];	
			}
				else {				//si est� en la lista abierta entonces comparamos las f's y si es menor entonces recalculamos
				if(posiciones[y][x].g + 10 < posiciones[y-1][x].g){
			posiciones[y-1][x].g=posiciones[y][x].g + 10; posiciones[y-1][x].h=calcularH(x,y-1,8,7); 
			posiciones[y-1][x].f=posiciones[y-1][x].g + posiciones[y-1][x].h;   posiciones[y-1][x].letra= tablero[y][x];
				}
			}
			}
			if(noreste() == true){
				if(comparar_strings2(x+1,y+1) == true){
			listaAbierta[cont2]=tablero[y+1][x+1];
			cont2++;
			posiciones[y+1][x+1].g=posiciones[y][x].g + 14; posiciones[y+1][x+1].h=calcularH(x+1,y+1,8,7); 
			posiciones[y+1][x+1].f=posiciones[y+1][x+1].g + posiciones[y+1][x+1].h;   posiciones[y+1][x+1].letra= tablero[y][x];	
			}
				else {				//si est� en la lista abierta entonces comparamos las f's y si es menor entonces recalculamos
				if(posiciones[y][x].g + 14 < posiciones[y+1][x+1].g){
			posiciones[y+1][x+1].g=posiciones[y][x].g + 14; posiciones[y+1][x+1].h=calcularH(x+1,y+1,8,7); 
			posiciones[y+1][x+1].f=posiciones[y+1][x+1].g + posiciones[y+1][x+1].h;   posiciones[y+1][x+1].letra= tablero[y][x];
				}
			}
			}
			if(noroeste() == true){
				if(comparar_strings2(x-1,y+1) == true){
			listaAbierta[cont2]=tablero[y+1][x-1];
			cont2++;
			posiciones[y+1][x-1].g=posiciones[y][x].g + 14; posiciones[y+1][x-1].h=calcularH(x-1,y+1,8,7); 
			posiciones[y+1][x-1].f=posiciones[y+1][x-1].g + posiciones[y+1][x-1].h;   posiciones[y+1][x-1].letra= tablero[y][x];	
			}
				else {				//si est� en la lista abierta entonces comparamos las f's y si es menor entonces recalculamos
				if(posiciones[y][x].g + 14 < posiciones[y+1][x-1].g){
			posiciones[y+1][x-1].g=posiciones[y][x].g + 14; posiciones[y+1][x-1].h=calcularH(x-1,y+1,8,7); 
			posiciones[y+1][x-1].f=posiciones[y+1][x-1].g + posiciones[y+1][x-1].h;   posiciones[y+1][x-1].letra= tablero[y][x];
				}
			}
			}
			if(sureste() == true){
				if(comparar_strings2(x+1,y-1) == true){
			listaAbierta[cont2]=tablero[y-1][x+1];
			cont2++;
			posiciones[y-1][x+1].g=posiciones[y][x].g + 14; posiciones[y-1][x+1].h=calcularH(x+1,y-1,8,7); 
			posiciones[y-1][x+1].f=posiciones[y-1][x+1].g + posiciones[y-1][x+1].h;   posiciones[y-1][x+1].letra= tablero[y][x];
			}
				else {				//si est� en la lista abierta entonces comparamos las f's y si es menor entonces recalculamos
				if(posiciones[y][x].g + 14 < posiciones[y-1][x+1].g){
			posiciones[y-1][x+1].g=posiciones[y][x].g + 14; posiciones[y-1][x+1].h=calcularH(x+1,y-1,8,7); 
			posiciones[y-1][x+1].f=posiciones[y-1][x+1].g + posiciones[y-1][x+1].h;   posiciones[y-1][x+1].letra= tablero[y][x];
				}
			}
			}
			if(suroeste() == true){
				if(comparar_strings2(x-1,y-1) == true){
				listaAbierta[cont2]=tablero[y-1][x-1];
			cont2++;
			posiciones[y-1][x-1].g=posiciones[y][x].g + 14; posiciones[y-1][x-1].h=calcularH(x-1,y-1,8,7); 
			posiciones[y-1][x-1].f=posiciones[y-1][x-1].g + posiciones[y-1][x-1].h;   posiciones[y-1][x-1].letra= tablero[y][x];
			}
				else {				//si est� en la lista abierta entonces comparamos las f's y si es menor entonces recalculamos
				if(posiciones[y][x].g + 14 < posiciones[y-1][x-1].g){
			posiciones[y-1][x-1].g=posiciones[y][x].g + 14; posiciones[y-1][x-1].h=calcularH(x-1,y-1,8,7); 
			posiciones[y-1][x-1].f=posiciones[y-1][x-1].g + posiciones[y-1][x-1].h;   posiciones[y-1][x-1].letra= tablero[y][x];
				}
			}
			}
			void Avanzar();
		}
	//	imprimirTablero2();
	}
}

bool izquierda(){
	if (tablero[y][x-1] == " " ||  comparar_strings(x-1,y) == false){
		return false;
	}else{
		return true;
	}	
}
bool derecha(){
		if (tablero[y][x+1] == " " ||  comparar_strings(x+1,y) == false){
		return false;
	}else{
		return true;
	}
}
bool arriba(){
		if (tablero[y+1][x] == " " ||  comparar_strings(x,y+1) == false){
		return false;
	}else{
		return true;
	}
}
bool abajo(){
		if (tablero[y-1][x] == " " ||  comparar_strings(x,y-1) == false){
		return false;
	}else{
		return true;
	}
}
bool noreste(){
		if (tablero[y+1][x+1] == " " ||  comparar_strings(x+1,y+1) == false){
		return false;
	}else{
		return true;
	}
}
bool noroeste(){
		if (tablero[y+1][x-1] == " " ||  comparar_strings(x-1,y+1) == false){
		return false;
	}else{
		return true;
	}
}
bool sureste(){
		if (tablero[y-1][x+1] == " " ||  comparar_strings(x+1,y-1) == false){
		return false;
	}else{
		return true;
	}
}
bool suroeste(){
		if (tablero[y-1][x-1] == " " ||  comparar_strings(x-1,y-1) == false){
		return false;
	}else{
		return true;
	}
}

void mover_izquierda(){
	x=x-1;
}
void mover_derecha(){
	x=x+1;
}
void mover_arriba(){
	y=y+1;
}
void mover_abajo(){
	y=y-1;
}
void mover_noreste(){
	x=x+1;
	y=y+1;
}
void mover_noroeste(){
	x=x-1;
	y=y+1;
}
void mover_sureste(){
	x=x+1;
	y=y-1;
}
void mover_suroeste(){
	x=x-1;
	y=y-1;
}

bool comparar_strings(int x, int y){  //comparar si ya esta en la lista Cerrada
	int i;
	for (i=0;i<100;i++){
		if(tablero[y][x] == listaCerrada[i]){
			return false;
		}
	}
	return true;
}

bool comparar_strings2(int x, int y){  //comparar si ya esta en la lista Abierta
	int i;
	for (i=0;i<100;i++){
		if(tablero[y][x] == listaAbierta[i]){
			return false;
		}
	}
	return true;
}

int calcularH(int x1, int y1, int x2,int y2){
	int cont = 0;
	while(x1 != x2){
		if(x1 > x2){
			x1--;
			cont++;
		}else{
			x1++;
			cont++;
		}
	}
		while(y1 != y2){
		if(y1 > y2){
			y1--;
			cont++;
		}else{
			y1++;
			cont++;
		}
	}
		
	return cont*G;
}

void Avanzar(){
		int i,j,k;
		int f_menor=10000, menor_x, menor_y,menor_k;
			for (i=0,j=0;i<9;i++){
		for(j=0;j<10;j++){
			for (k=0;k<100;k++){
				if(tablero[i][j]==listaAbierta[k]){
					if(posiciones[i][j].f < f_menor){
						menor_x=i;
						menor_y=j;
						menor_k=k;
						f_menor=posiciones[i][j].f;
					}
				}
			}
		}
	}
	listaAbierta[menor_k]=NULL; 					//eliminamos de la lista Abierta
	listaCerrada[cont]=tablero[menor_x][menor_y];  //agregamos a la lista cerrada
	cont++;
	x=menor_x;			//Actualizamos la posicion actual
	y=menor_y;
}
